import os.path
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import sleep
from typing import List

from txt_processing import replace_variables
from utils import Progress


class SMTPConfig:

    def __init__(self,
                 host: str,
                 port: int,
                 username: str,
                 password: str,
                 from_email: str,
                 mails_per_second: float,
                 ):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.from_email = from_email
        self.mails_per_second = mails_per_second


class SMTPService:

    def __init__(self, config: SMTPConfig):
        self.from_email = config.from_email
        self.timout = 1 / config.mails_per_second
        self.smtp = smtplib.SMTP(host=config.host, port=config.port)
        self.smtp.starttls()
        self.smtp.login(config.username, config.password)

    def close(self):
        self.smtp.close()

    def send_mail(self, subject: str, receiver: str, message: str, attachment: str):
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = self.from_email
        msg['To'] = receiver
        msg.attach(MIMEText(message, _charset='utf-8'))

        with open(attachment, 'rb') as file:
            basename = os.path.basename(attachment)
            part = MIMEApplication(file.read(), Name=basename)
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename
        msg.attach(part)

        self.smtp.sendmail(self.from_email, receiver, msg.as_string())

    def send_mails(self, subject: str, receivers: List[str], message: str, attachments: List[str]):
        assert len(receivers) == len(attachments)
        progress = Progress(len(receivers), "Sending mails")
        for idx, receiver in enumerate(receivers):
            progress.update(idx)
            email = replace_variables(message,
                                      first_name=receiver[0],
                                      last_name=receiver[1],
                                      email=receiver[2])
            self.send_mail(
                subject=subject,
                receiver=receiver[2],
                message=email,
                attachment=attachments[idx]
            )
            sleep(self.timout)

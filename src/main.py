import getopt
import sys
from os import makedirs
from os.path import join
from shutil import rmtree

from csv_processing import csv_processing
from pdf_processing import pdf_processing
from smtp import SMTPConfig, SMTPService
from txt_processing import txt_load
from utils import read_yaml, absolute_path


def cli(pwd, args):
    arg1 = ("-c", "receivers")
    arg2 = ("-t", "text")
    arg3 = ("-f", "pdf_front")
    arg4 = ("-b", "pdf_back")
    usage = "pdf-mailer %s <%s> %s <%s> %s <%s> %s <%s>" % (*arg1, *arg2, *arg3, *arg4)
    try:
        opts, args = getopt.getopt(args, "hc:t:f:b:", [arg1[1], arg2[1], arg3[1], arg4[1]])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    receivers = text = pdf_frontsides = pdf_backsides = None

    for opt, arg in opts:
        print(opt, arg)
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in (arg1[0], "--%s" % arg1[1]):
            receivers = absolute_path(arg, pwd)
            print("Path to receivers: %s" % receivers)
        elif opt in (arg2[0], "--%s" % arg2[1]):
            text = absolute_path(arg, pwd)
            print("Path to text: %s" % text)
        elif opt in (arg3[0], "--%s" % arg3[1]):
            pdf_frontsides = absolute_path(arg, pwd)
            print("Path to pdf frontsides: %s" % pdf_frontsides)
        elif opt in (arg4[0], "--%s" % arg4[1]):
            pdf_backsides = absolute_path(arg, pwd)
            print("Path to pdf backsides: %s" % pdf_backsides)

    if receivers is None or text is None or pdf_frontsides is None or pdf_backsides is None:
        print("one or all parameters was wrong")
        print(usage)
        sys.exit(2)

    return receivers, text, pdf_frontsides, pdf_backsides


def execute(path_to_csv: str, path_to_txt: str, path_to_pdf_frontsides: str, path_to_pdf_backsides: str, temp_dir: str,
            config_path: str):
    makedirs(temp_dir, exist_ok=True)
    receivers = csv_processing(path_to_csv)
    subject, email = txt_load(path_to_txt)
    pdfs = pdf_processing(path_to_pdf_frontsides, path_to_pdf_backsides, path_to_csv, temp_dir)

    smtp_config = SMTPConfig(**read_yaml(config_path)["smtp"])
    smtp_service = SMTPService(smtp_config)
    smtp_service.send_mails(
        receivers=receivers,
        subject=subject,
        message=email,
        attachments=pdfs
    )
    smtp_service.close()
    rmtree(temp_dir)


def main():
    USER_DIR = sys.argv[1]
    TEMP_DIR = join(USER_DIR, "tmp")
    CONFIG = join(USER_DIR, "config.yaml")
    receivers, text, pdf_frontsides, pdf_backsides = cli(sys.argv[2], sys.argv[3:])
    execute(
        path_to_csv=receivers,
        path_to_txt=text,
        path_to_pdf_frontsides=pdf_frontsides,
        path_to_pdf_backsides=pdf_backsides,
        temp_dir = TEMP_DIR,
        config_path=CONFIG
    )

if __name__ == "__main__":
    main()

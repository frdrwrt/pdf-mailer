from os.path import basename, join
from typing import List

from PyPDF2 import PdfFileWriter, PdfFileReader

from csv_processing import csv_processing
from utils import Progress


def pdf_processing(filepath_to_frontsides: str, filepath_to_backsides: str, filepath_to_email_receivers: str,
                   temp_dir: str) -> List[str]:
    frontsides = PdfFileReader(open(filepath_to_frontsides, "rb"))
    backsides = PdfFileReader(open(filepath_to_backsides, "rb"))
    receivers = csv_processing(filepath_to_email_receivers)

    if frontsides.numPages != backsides.numPages:
        raise ValueError("Number of frontsides must be identically to the number of backsides.")
    if len(receivers) > frontsides.numPages:
        raise ValueError("There can't be more receivers (in your csv) than number of pdf pages (in your pdfs).")

    new_files = []
    num_receivers = len(receivers)
    current_pdf_page = 0
    progress = Progress(num_receivers, "Processing receivers:")
    for idx, receiver in enumerate(receivers):
        progress.update(idx)
        output = PdfFileWriter()
        if receiver[3] == "one_page":
            output.addPage(frontsides.getPage(current_pdf_page))
            current_pdf_page = current_pdf_page + 1
        elif receiver[3] == "two_pages_separate":
            output.addPage(frontsides.getPage(current_pdf_page))
            output.addPage(frontsides.getPage(current_pdf_page + 1))
            current_pdf_page = current_pdf_page + 2
        elif receiver[3] == "two_pages_eco":
            output.addPage(frontsides.getPage(current_pdf_page))
            output.addPage(backsides.getPage(current_pdf_page))
            current_pdf_page = current_pdf_page + 1
        else:
            raise ValueError("Unknown case %s for %s %s. Supported cases are: one_page, two_pages_separate, "
                             "two_pages_eco"
                             % (receiver[3], receiver[0], receiver[1]))
        # receiver[0] is first name, receiver[1] is the last name
        new_file = join(temp_dir, "%s-%s.pdf" % (receiver[0], receiver[1]))
        new_files.append(new_file)
        with open(new_file, "wb") as outputStream:
            output.write(outputStream)

    return new_files

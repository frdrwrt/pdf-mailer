def txt_load(file_path: str) -> (str, str):
    with open(file_path, ) as file:
        txt = file.read()
    return tuple(txt.split("\n", 1))


def txt_replace_variable(txt: str, key: str, value: str) -> str:
    return txt.replace("${%s}" % key, value)

def replace_variables(txt: str, first_name:str, last_name:str, email:str):
    txt = txt_replace_variable(txt, "first_name", first_name)
    txt = txt_replace_variable(txt, "last_name", last_name)
    return txt_replace_variable(txt, "email", email)


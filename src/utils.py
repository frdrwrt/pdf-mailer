import sys
from os import makedirs
from os.path import join
from shutil import rmtree

import yaml


def read_yaml(file_path):
    with open(file_path, 'r') as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return data


def absolute_path(path: str, pwd: str) -> str:
    if path[0] is not "/":
        path = join(pwd, path)
    return path


class Progress:
    def __init__(self, total: int, decription: str):
        self.total = total
        self.steps = self.total / 100
        self.description = decription
        self.length = 0

    def update(self, progress: int):
        progress += 1
        percentage = int(progress / self.total * 100)
        string = "\t%s %i of %i [%i%%]" % (self.description, progress, self.total, percentage)
        sys.stdout.write("\b" * self.length)
        sys.stdout.write(string)
        if progress / self.total >= 1:
            sys.stdout.write('\n')
        else:
            sys.stdout.flush()
        self.length = len(string)


def temp_dir(func, path="tmp"):
    def decorator(*args, **kwargs):
        makedirs(path)
        func(*args, **kwargs)
        rmtree(path)

from typing import List

import pandas as pd

def csv_processing(file_path: str) -> List:
    receiver_list = pd.read_csv(file_path,
                                sep=";",
                                encoding="utf-8",
                                header=None,
                                skiprows=None,
                                skip_blank_lines=True,
                                skipinitialspace=True,
                                dtype=str).values

    return receiver_list.tolist()

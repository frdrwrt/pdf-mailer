#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

home_dir=$HOME/.pdf-mailer
opt_dir=/opt/pdf-mailer

echo "=> actviate venv"
source $home_dir/venv_pdf-mailer/bin/activate
PYTHONPATH=$opt_dir/src
echo "=> start pdf-mailer"
python $opt_dir/src/main.py $home_dir $(pwd) $@

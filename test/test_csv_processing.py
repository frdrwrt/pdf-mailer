from csv_processing import csv_processing


def test_csv_processing(email_receivers_csv):
    email_receivers = csv_processing(email_receivers_csv)

    assert email_receivers == [
        ["Alter", "Schwede", "alter_schwede@gmx.net", "one_page"],
        ["Opa", "Rudolf", "superopa@gmail.com", "two_pages_separate"],
        ["Axel", "Schweiß", "bananenwein@posteo.net", "two_pages_eco"],
        ["Otto", "Walkes", "ottowalkes@gmail.com", "two_pages_separate"],
        ["Theo", "Tester", "theotester@mail.org", "one_page"]
    ]
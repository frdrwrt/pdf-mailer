from os import makedirs
from os.path import join
from shutil import rmtree

from pytest import fixture

from smtp import SMTPConfig, SMTPService
from utils import read_yaml

"""
For simplisity you need to check the result of these tests on your own.
Get a mailbox at mailtrap.com and the smtp server configuration to this test.
"""
@fixture
def smtp_service(config_yaml):
    smtp_config = SMTPConfig(**read_yaml(config_yaml)["smtp"])
    return SMTPService(smtp_config)

@fixture
def files_path():
    return "test/files"

@fixture
def config_yaml(files_path):
    return join(files_path, "config.yaml")

@fixture
def email_receivers_csv(files_path):
    return join(files_path, "email_receivers.csv")

@fixture
def email_txt(files_path):
    return join(files_path, "email.txt")

@fixture
def pdf_frontsides(files_path):
    return join(files_path, "pdfs_frontsides.pdf")

@fixture
def pdf_backsides(files_path):
    return join(files_path, "pdfs_backsides.pdf")



@fixture
def tmp_dir():
    tmp_dir = "tmp"
    makedirs(tmp_dir, exist_ok=True)
    yield tmp_dir
    rmtree(tmp_dir, ignore_errors=True)
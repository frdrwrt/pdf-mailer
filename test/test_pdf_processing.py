from os import makedirs
from os.path import exists
from shutil import rmtree

from PyPDF2 import PdfFileReader
from pdf_processing import pdf_processing


def test_pdf_processing(pdf_frontsides, pdf_backsides, email_receivers_csv):
    PATH_TEMP_DIR = "tmp"
    makedirs(PATH_TEMP_DIR, exist_ok=True)

    pdf_list = pdf_processing(pdf_frontsides, pdf_backsides, email_receivers_csv, PATH_TEMP_DIR)
    assert len(pdf_list) == 5
    for pdf in pdf_list:
        assert exists(pdf)

    pdf_to_test = pdf_list[0] # should be one_side for Alter Schwede
    assert "Schwede" in get_filename_from_path(pdf_to_test)
    assert PdfFileReader(open(pdf_to_test, "rb")).numPages == 1

    pdf_to_test = pdf_list[1] # should be two_sides_separate for Opa Rudolf
    assert "Rudolf" in get_filename_from_path(pdf_to_test)
    assert PdfFileReader(open(pdf_to_test, "rb")).numPages == 2

    pdf_to_test = pdf_list[2] # should be two_sides_eco for Axel Schweiß
    assert "Schweiß" in get_filename_from_path(pdf_to_test)
    assert PdfFileReader(open(pdf_to_test, "rb")).numPages == 2

    pdf_to_test = pdf_list[3] # should be two_sides_separate for Otto Walkes
    assert "Walkes" in get_filename_from_path(pdf_to_test)
    assert PdfFileReader(open(pdf_to_test, "rb")).numPages == 2

    pdf_to_test = pdf_list[4] # should be one_side for Theo Tester
    assert "Tester" in get_filename_from_path(pdf_to_test)
    assert PdfFileReader(open(pdf_to_test, "rb")).numPages == 1

    rmtree(PATH_TEMP_DIR)

def get_filename_from_path(filepath: str):
    return filepath.split("/")[-1]
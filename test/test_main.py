from os.path import join

from pytest import mark

from main import main, cli, execute


class TestMain():

    @mark.skip
    def test_main(cli):
        main()

    def test_cli(self, email_receivers_csv, email_txt, pdf_frontsides, pdf_backsides):
        args = ["-c", email_receivers_csv, "-t", email_txt, "-f", pdf_frontsides, "-b", pdf_backsides]
        receivers, txt, pdf_frontsides, pdf_backsides = cli("/", args)
        assert receivers == join("/", email_receivers_csv)
        assert txt == join("/", email_txt)
        assert pdf_frontsides == join("/", pdf_frontsides)
        assert pdf_backsides == join("/", pdf_backsides)

    def test_execute(self, email_receivers_csv, email_txt, pdf_frontsides, pdf_backsides, config_yaml, tmp_dir):
        execute(
            path_to_csv=email_receivers_csv,
            path_to_txt=email_txt,
            path_to_pdf_frontsides=pdf_frontsides,
            path_to_pdf_backsides=pdf_backsides,
            temp_dir=tmp_dir,
            config_path=config_yaml
        )

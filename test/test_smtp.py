class TestSMTP:

    def test_send_mail(self, smtp_service, pdf_frontsides):

        smtp_service.send_mail(
            subject="Subject",
            receiver="test@test.com",
            message="testmail",
            attachment=pdf_frontsides
        )

    def test_send_mails(self, smtp_service, email_txt, email_receivers_csv, pdf_frontsides):
        smtp_service.send_mails(
            subject="TESTMAIL",
            receivers=["testA@gmx.net", "testB@gmail.com", "testC@keinplan.eu"],
            message="testmail",
            attachments=[email_txt, email_receivers_csv, pdf_frontsides]
        )
from os.path import join

from utils import Progress, absolute_path


class TestUtils:

    def test_progress(self):
        progress = Progress(400, "steps")
        for i in range(400):
            progress.update(i)
        print("sysout after progress")

    def test_absolute_path(self):
        pwd = "/some/path"
        result = absolute_path("somedir/somewhere", pwd)
        assert result == join(pwd, "somedir/somewhere")
        result = absolute_path("/absolute/path", pwd)
        assert result == "/absolute/path"

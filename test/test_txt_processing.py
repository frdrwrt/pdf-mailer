from txt_processing import txt_load, txt_replace_variable


def test_txt_load(email_txt):
    filepath = email_txt
    subject, txt = txt_load(filepath)
    assert subject == "Subject"
    assert txt == """Hey ${first_name} ${last_name},

your email is ${email}.

cheers"""


def test_txt_replace_variable():
    txt = "Hallo ${name}!"
    new = txt_replace_variable(txt, "name", "Simon")
    assert new == "Hallo Simon!"

# PDF MAILER
pdf mailer takes a list of email receivers and a pdf with pages for every receiver. pdf mailer splits the pdf and sends 
every receiver a email with the corresponding pdf as attachment.
## Installation
1. Download the installation script and run it
```shell script
wget -O - https://gitlab.com/frdrwrt/pdf-mailer/raw/master/scripts/install.sh?inline=false | bash
```

### setup smtp configurations
2. Enter your smtp configuration of your smtp server
```shell script
cd $HOME/.pdf-mailer
vim config.yaml
```

## Usage
To use the pdf mailer you need to provide 4 files (filenames do not matter):
1. **email-receivers.csv:** the csv file must include four columns in the following order first name;last name;
email address, case without header rows, separated with ";" and utf-8 encoded.  
There are three cases at the moment that lead to different attachments:
1) *one_page*: Attachment contains one pdf side coming from *pdf_frontsides.pdf*
2) *two_pages_separate*: Attachment contains two pdf sides. Both are extracted from *pdf_frontsides.pdf*.
3) *two_pages_eco*: Attachment contains two pdf sides. One is extracted from *pdf_frontsides.pdf*, the other one from
*pdf_backsides.pdf*
Example:
```csv
peter;lustig;peter-lustig@gmx.net;one_page
johnny;depp;johnnydepp@gmail.com;two_pages_eco
``` 
2. **email.txt:** 
the text of the email. If you want to include first name, last name, or email address of the receiver you can use these templates
${first_name}, ${last_name}, ${email}  
Example:
```text
Hallo ${first_name} ${last_name},
your email address is ${email}
```
3. **pdf_frontsides.pdf:** 
one pdf that contains the front sides of a multilateral scan

4. **pdf_backsides.pdf:**
one pdf that contains the back sides of a multilateral scan. Its number of pages has to be equal to 
*pdf_frontsides.pdf*'s number of pages.

To start pdf mailer run following command from your terminal:
```shell script
pdf-mailer -c <path/to/receivers.csv> -t <path/to/email.txt> -f <path/to/pdf_front.pdf> -b <path/to/pdf_back.pdf>
```
## Development
it is recommended to use virtualenv with python3 on your system.

### Mailtrap
to test with real smtp server mailtrap works pretty well (https://mailtrap.io)
### Vagrant
vagrant is used to test and develop the installation/deployment process on clean systems.
check [https://www.vagrantup.com/intro/index.html] for more information.
```shell script
cd pdf-mailer
vagrant up
```


